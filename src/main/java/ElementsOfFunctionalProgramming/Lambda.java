package ElementsOfFunctionalProgramming;

interface Even {
    boolean IsEven(int t);
}
class SumArray {
    public int Sum(int[] A, Even refLambda) {
        int sum=0;
        for (int i=0; i<A.length; i++)
            if (refLambda.IsEven(A[i]))
                sum+=A[i];
        return sum;
    }
}

public class Lambda {

    public static void main(String[] args) {

        Even ref1;
        ref1 = (t) -> {
            if (t%2==0) return true;
            else return false;
        };

        SumArray arraySum = new SumArray();

        int[] A = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 0 };
        int sum = arraySum.Sum(A, ref1);
        System.out.println("Even sum = " + sum);

        sum = arraySum.Sum(A,
                (Even)((t) -> {
                    if (t%2!=0) return true;
                    else return false;
                }));
        System.out.println("Odd sum = " + sum);
    }
}
