package ElementsOfFunctionalProgramming;

import java.util.ArrayList;
import java.util.List;

public class EvenFilter {
    public static void main(String[] args) {

        int[] array = new int[10];

        for (int i = 0; i < array.length; i++) {
            array[i] = (int) (Math.random() * 10);
        }
        List<Integer> listNumbers = new ArrayList<>();

        for (Integer numbers : array) {
            listNumbers.add(numbers);
        }
        System.out.println(listNumbers);
        long evenNumber = listNumbers.stream()
                .filter(i -> i % 2 == 0)
                .count();
        System.out.println(evenNumber);
    }
}
