package ElementsOfFunctionalProgramming;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class SumOfSquares {
    public static void main(String[] args) {

        int[] array = new int[10];
        for (int i = 0; i < array.length; i++) {
            array[i] = (int) (Math.random() * 10);
        }
        List<Integer> listNumbers = new ArrayList<>();

        for (Integer numbers : array) {
            listNumbers.add(numbers);
        }
        System.out.println(listNumbers);
        Optional<Integer> allSum = listNumbers.stream()
                .map(a -> a * a)
                .reduce(Integer::sum);
        System.out.println(allSum);


    }
}
