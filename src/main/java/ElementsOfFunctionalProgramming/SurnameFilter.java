package ElementsOfFunctionalProgramming;

import java.util.ArrayList;
import java.util.List;

public class SurnameFilter {
    public static void main(String[] args) {
        String[] surnamesArray = {"Marshall","Cole","Jackson","Martines","Reeves"};
        List<String> listSurnames =new ArrayList<>();
        for (String surnames : surnamesArray) {
            listSurnames.add(surnames);
        }
        listSurnames.stream()
                .filter(s -> s.startsWith("J"))
                .forEach(System.out::println);
    }
}
